import { Router } from 'express';

export const RootRouter: Router = Router();

RootRouter.route('')
  .options(function(): void {
    arguments[1].status(204).send();
  }) // Todo: Options method handler
  .all(function(): void {
    arguments[1].status(404).json({ description: 'Not Found' });
  });
