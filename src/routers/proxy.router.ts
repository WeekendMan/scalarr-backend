import { Router, Request, Response } from 'express';
import * as http from 'http';
import * as https from 'https';
import { IncomingMessage } from 'http';

export const ProxyRouter: Router = Router();

ProxyRouter.route('')
  .post((request: Request, response: Response): void => {
    if (!!request.body.url) {
      const getter: Function = (
        request.body.url.search('https://') === 0
          ? https
          : http
      ).get;

      getter(
        request.body.url,
        (res: IncomingMessage): void => {
          let content: string = '';

          res.on('data', (chunk: any): void => {
            content += chunk;
          });

          res.on('end', (): void => {
            response
              .status(res.statusCode || 200)
              .send(content);
          });
        }
      ).on('error', (error: Error): void => {
        response
          .status(500)
          .json({ description: error.message });
      });
    } else {
      response
        .status(400)
        .json({ description: 'Correct URL is required in "url" request payload' });
    }  
  }) // Todo: Return user's model if exists
  .options(function(): void {
    arguments[1].status(204).send();
  }) // Todo: Options method handler
  .all(function(): void {
    arguments[1]
      .status(405)
      .json({ description: 'Method not allowed' })
  }); // Todo: Return error "Method is not implemented"

ProxyRouter.all('*', function(): void {
  arguments[1].status(404).json({ description: 'Not Found' });
});

