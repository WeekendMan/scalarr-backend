import { Response, Request } from "express";
import { NextFunction } from "connect";

export class HTTPHelper {
  public static setCORSHeaders(request: Request, response: Response, next: NextFunction): void {
    response.setHeader(
      'Access-Control-Allow-Origin',
      request.get('origin') || ''
    );
    response.setHeader(
      'Access-Control-Allow-Credentials',
      'true'
    );
    response.setHeader(
      'Access-Control-Allow-Headers',
      'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range'
    );
    response.setHeader(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PATCH, PUT, DELETE'
    );
    response.setHeader(
      'Access-Control-Expose-Headers',
      'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range'
    );

    next();
  }
}
