import 'reflect-metadata';
import { Express } from 'express';
import * as Core from 'express';
import * as BodyParser from 'body-parser';
import { HTTPHelper } from './helpers/http.helper';
import { ProxyRouter } from './routers/proxy.router';
import { RootRouter } from './routers/root.router';


// Getting Express app instance
const app: Express = Core();

// Defining debug port if exists
process.debugPort = Number.parseInt(process.env.DEBUG_PORT || '', 10);

// Defining app port (defined / default)
app.set(
  'port',
  (
    !!process.env.PORT &&
    !isNaN(Number.parseInt(process.env.PORT, 10)) &&
    Number.parseInt(process.env.PORT, 10) > 0 &&
    Number.parseInt(process.env.PORT, 10) < 65537
      ? Number.parseInt(process.env.PORT, 10)
      : 9000
  )
);

// Express data parsers
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(HTTPHelper.setCORSHeaders);

// Routes
app.use('/api/proxy', ProxyRouter);
app.use('/', RootRouter);

// Starting app
app.listen(
  app.get('port'),
  (): void => console.log('App started!')
);
